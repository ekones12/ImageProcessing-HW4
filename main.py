from part1 import *
import argparse

def image_segmentation(partno,segment,cluster):
    ims=["iowa","owens_valley","salt_lake"]
    for i in range(3):

        path = "results/"+"part"+str(partno)+"/" + ims[i] + "/"
        createFolder(path)
        image = infareds(ims[i])
        if partno==1:
            im1 = cv2.merge([image["Blue"],image["Green"],image["Red"]])
        if partno ==2:
            infared_R = image["Infared"]
            infared_G = np.uint8((image["Green"] + image["Red"])/2)
            infared_B=  np.uint8((image["Blue"] + image["Green"])/2)
            im1 = cv2.merge((infared_B,infared_G,infared_R))

        cv2.imwrite(path+ims[i]+"_rgb.jpg",im1)

        segmentation(im1,segment,cluster,path)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-superpixel", required=True,
                    help="superpixel size")
    ap.add_argument("-kmean", required=True,
                    help="kmean cluster size")
    args = vars(ap.parse_args())

    image_segmentation(1,int(args["superpixel"]),int(args["kmean"]))
    image_segmentation(2,int(args["superpixel"]),int(args["kmean"]))