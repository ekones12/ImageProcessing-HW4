from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import cv2
import numpy as np
import os


def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print('Error: Creating directory. ' + directory)


def readfile_gray(imagepath):

    image = cv2.imread(imagepath,0)

    return image



# transform to np.uint8 format
def uint8(image):

    return np.uint8(np.clip(image * 255, 0, 255))


def normalization(image):
    cv2.normalize(image, image, 0, 255, cv2.NORM_MINMAX)
    return image


def infareds(folderpath):
    imagesdict = {}

    path ="data/" + folderpath + "/" + folderpath +"-band"
    imagesdict["Blue"] = readfile_gray(path + "1.tif")
    imagesdict["Green"] = readfile_gray(path + "2.tif")
    imagesdict["Red"] = readfile_gray(path + "3.tif")
    imagesdict["Infared"] = readfile_gray(path + "4.tif")

    return imagesdict
