from readfile import *

def segmentation(image,numSegments,kmean,path):
    path = path + "segments_" + str(numSegments) + "/" + "kmean_" + str(kmean)+"/"
    createFolder(path)
    segments = slic(image, n_segments=numSegments,sigma=3)
    mean_segments=np.zeros((segments.max()+1,4))
    lab_image = cv2.cvtColor(image,cv2.COLOR_BGR2LAB)[:,:,0]
    for i in range(segments.max()+1):
        mean_segments[i][0] = np.mean(image[np.where(segments == i)])
        mean_segments[i][1] = np.std(image[np.where(segments == i)])
        mean_segments[i][2] = np.mean(lab_image[np.where(segments == i)])
        mean_segments[i][3] = np.sum(np.histogram(image[np.where(segments==i)])[0])
    cv2.imwrite(path+"segmented.jpg",normalization(mark_boundaries(image,segments)))

    plt.imsave(path+"segment_labels.jpg",segments)
    mean_segments /= np.max(mean_segments,axis=0)
    enes= KMeans(n_clusters=kmean)
    enes.fit(mean_segments)

    kmeans= segments.copy()
    for i in range(segments.max()+1):
        kmeans[np.where(segments==i)]= enes.labels_[i]

    cv2.imwrite(path + "kmean.jpg", normalization(mark_boundaries(image, kmeans)))
    plt.imsave(path + "kmean_labels.jpg", kmeans)




